package com.easynetty.core.net.listener;

import com.easynetty.core.net.Client;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

/**
 * Created by Mengyue on 2016-07-22.
 */
public class ConnectionListener implements ChannelFutureListener {
    private Client client;

    public ConnectionListener(Client client) {
        this.client = client;
    }

    public void operationComplete(ChannelFuture channelFuture) throws Exception {
        if (!channelFuture.isSuccess()) {
            System.out.println("Reconnect");
            Thread.sleep(25000);
            client.reConnect();
        }
    }
}